<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 10:00
 */
abstract class AbstractModel {

    protected $tableName;
    protected $db;
    protected $data;

    public function addData($name, $value)
    {
        $this->data[$name] = strip_tags($value);
    }

    public function getData($name = null)
    {
        if($name){
           return $this->data[$name];
        }
        return $this->data;
    }

    public function __construct($id = null)
    {
        $this->db = new Db($this->tableName);
        //simple object generator
        if($id) {
            $data = $this->db->loadData($id);
            foreach($data as $filed => $value) {
                $this->addData($filed, $value);
            }
        }
    }

    public function getList($params = array())
    {
        return $this->db->getTableSortedList($params);
    }

    public function save()
    {
        return $this->db->saveTableData($this->getData());
    }


}