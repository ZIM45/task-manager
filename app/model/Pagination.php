<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 17.06.2020
 * Time: 11:49
 */
class Pagination {

    protected $model;
    protected $limit;
    protected $page;
    protected $data;
    protected $total;

    /**
     * Pagination constructor.
     * $model = instanse of AbstractModel
     */
    public function __construct($model)
    {
        $this->model = $model;
        $this->limit = 3;
    }

    /**
     * @param $params array of params to sort collection of data
     * page, sort, direction
     * @return array
     */
    public function getPaginationList($params)
    {
        $data['data'] = $this->model->getList($params);
        $data['page'] = $params['page'];
        $data['per_page'] = $this->limit;
        $data['total'] = count($this->model->getList());
        $data['total_pages'] = ceil($data['total'] / $this->limit);
        return $data;
    }

}