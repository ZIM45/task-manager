<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 11:51
 */
class Task extends AbstractModel
{
    /**
     * Task constructor.
     */
    public function __construct($id = null)
    {
        $this->tableName = 'task';
        parent::__construct($id);
    }
}