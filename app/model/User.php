<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 11:50
 */

class User extends AbstractModel {

    public function __construct($id = null)
    {
        $this->tableName = 'user';
        parent::__construct($id);
    }

    public function ifExistByPass($username, $password)
    {
        $result = $this->db->findInTable(array('login'=> $username, 'password' => md5($password)));
        return $result[0]['id'];
    }

}