<?php
class Db {

    protected $connection;
    protected $tableName;

    public function __construct($tableName = null)
    {
        $this->tableName = $tableName;
        $this->connection = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        if ($this->connection->connect_error) {
            echo $this->connection->error;
            die();
        }
    }

    public function getConnection()
    {
        return $this->getConnection();
    }

    public function __destruct()
    {
        if ($this->connection) {
            $this->connection->close();
        }
    }

    public function loadData($id)
    {
        $sql = 'SELECT * FROM '.$this->tableName;
        if($id) {
            $sql.= ' WHERE id = "'.$id.'"';
        }
        $response = $this->query($sql);
        return $response[0];
    }

    public function getTableSortedList($params = array())
    {
        $query = 'SELECT * FROM '.$this->tableName;

        if($params['sort']){
            $query.= ' ORDER BY '.$params['sort'] . ' ' . (($params['dir']) ? $params['dir'].' ' : ' ASC ');
        }
        if($params['limit']){
            $query.= 'LIMIT '.$params['limit'].' ';
        }

        if($params['page']){
            $query.= 'OFFSET '.(($params['page'] - 1) * $params['limit']);
        }
        return $this->query($query);
    }

    public function saveTableData($data)
    {
        $sql = "INSERT INTO ".$this->tableName;
        if($data['id']) {
            $sql = "REPLACE INTO ".$this->tableName;
        }
        $columns = $this->getTableColumns();
        $forUpdateData = [];
        foreach($data as $name => $value) {
            if(in_array($name, $columns) && !empty($value)){
                $forUpdateData[$name] = $value;
            }
        }
        $sql.= ' ('.implode(',', array_keys($forUpdateData)).') VALUES (';
        foreach($forUpdateData as $key => $value) {
            $sql.= "'".$value."'";
            $comma = (array_search($key, array_keys($forUpdateData))+1 < count($forUpdateData)) ? "," : "";
            $sql.= $comma;
        }
        $sql.= ')';
        return $this->query($sql);
    }

    public function getCount()
    {
        $result = $this->query('SELECT COUNT(*) as total FROM '.$this->tableName);
        return $result[0]['total'];
    }

    /**
     * @param array $params
     * find in db table by column name and value $params = ['columnName' => 'columnsValue']
     */
    public function findInTable($params = array())
    {
        $sql = "SELECT * FROM ".$this->tableName." WHERE ";
        foreach($params as $name => $value){
            $and = (array_search($name, array_keys($params))+1 < count($params)) ? " AND " : "";
            $sql.= $name. " = '".$value."'".$and;
        }

        return $this->query($sql);
    }

    protected function getTableColumns()
    {
        $columns = [];
        $sql = 'SHOW COLUMNS FROM '.$this->tableName;
        $result = $this->query($sql);
        foreach($result as $item){
            $columns[] = $item['Field'];
        }
        return $columns;
    }

    protected function query($sql)
    {
        $data = [];
        if ($result = $this->connection->query($sql)) {

            if(!$result->num_rows) {
                return $result;
            }

            while ($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
            $result->close();
        }

        return $data;
    }



}