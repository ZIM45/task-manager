$(document).ready(function() {
    $("#updateTaskForm").submit(function(e){
        e.preventDefault()
    })
    $('#updateTaskForm').on('submit', function(e) {
        var checkEmptyInput = $(this).find("input[type=text]:visible").filter(function() { if($(this).val()=="") return $(this); }).length;
        if(checkEmptyInput > 0) {
            alert('Не заполнены обязательные поля');
            $(this).find("input[type=text]:visible").filter(function() { if($(this).val()=="") return $(this); })[0].focus();
            return false;
        }
        $.ajax({
            url: 'ajax/task/edit',
            type: 'post',
            dataType: 'json',
            data: $("#updateTaskForm").serialize(),
            success: function(json) {
                alert(json.message);
                $(function () {
                    $('#modalTaskEdit').modal('toggle');
                });
            }
        });
    });

    $("#createTaskForm").submit(function(e){
        e.preventDefault()
    })
    $('#createTaskForm').on('submit', function(e) {
        var checkEmptyInput = $(this).find("input[type=text]:visible").filter(function() { if($(this).val()=="") return $(this); }).length;
        if(checkEmptyInput > 0) {
            alert('Не заполнены обязательные поля');
            $(this).find("input[type=text]:visible").filter(function() { if($(this).val()=="") return $(this); })[0].focus();
            return false;
        }

        $.ajax({
            url: 'ajax/task/save',
            type: 'post',
            dataType: 'text',
            data: $("#createTaskForm").serialize(),
            complete: function() {
                alert('Задача успешно сохранена');
                $(function () {
                    $('#modalTaskAdd').modal('toggle');
                });
            }
        });
    });

    $("#autorizationForm").submit(function(e){
        e.preventDefault()
    })
    $('#autorizationForm').on('submit', function(e) {
        var checkEmptyInput = $(this).find("input[type=text]:visible").filter(function() { if($(this).val()=="") return $(this); }).length;
        if(checkEmptyInput>0) {
            alert('Не заполнены обязательные поля или введенные данные не верные');
            $(this).find("input[type=text]:visible").filter(function() { if($(this).val()=="") return $(this); })[0].focus();
            return false;
        }
        $.ajax({
            url: '/ajax/user/auth',
            type: 'post',
            dataType: 'json',
            data: $("#autorizationForm").serialize(),
            success: function(json) {
                if(json.status == 'success') {
                    alert('Успешная авторизация');
                    $(function () {
                        $('#autorizationUserModal').modal('toggle');
                    });
                    location.reload();
                } else {
                    alert('Не заполнены обязательные поля или введенные данные не верные');
                }
            }
        });
    });

    $('#logoutButton').on('click', function(event) {
        event.preventDefault();
        $.ajax({
            url: '/ajax/user/logout',
            type: 'post',
            dataType: 'json',
            complete: function(json) {
               location.reload();
            }
        });
    });


    $('.completeButton').on('click', function(event) {
        $.ajax({
            url: '/ajax/task/changeComplete',
            type: 'post',
            dataType: 'json',
            data: {'id': $(this).attr("data-id")},
            complete: function(json) {
                alert('Статус задачи обновлен');
            }
        });
    });

    $('.editButton').on('click', function(event) {
        console.log($(this).attr("data-id"));
        $.ajax({
            url: '/ajax/task/getTask',
            type: 'get',
            dataType: 'json',
            data: {'id': $(this).attr("data-id")},
            success: function(json) {
                $.each(json, function(key, element) {
                    $('#updateTaskForm input').each(
                        function(index){
                            var input = $(this);
                            if(input.attr('name') == key) {
                                input.val(element);
                                console.log(element);
                            }
                        }
                    );
                });
                $(function () {
                    $('#modalTaskEdit').modal('show');
                });
            }
        });
    });
});

