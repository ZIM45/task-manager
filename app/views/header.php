<!DOCTYPE html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<link rel="stylesheet" type="text/css"  href="/app/skin/style.css" >
<script src="/app/skin/scripts.js"></script>

<div class="alert alert-success hidden"><strong>Успешная операция!</strong><span></span></div>

<div class="modal fade" id="modalTaskEdit" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Редактировать задачу</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Редактировать задачу">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="updateTaskForm" role="form" method="POST" data-method="create" action="">
                <div class="modal-body mx-3">
                    <input type="hidden" id="defaultForm-text" name="id" class="form-control validate" value="" >
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input type="text" id="defaultForm-text" name="user_name" class="form-control validate" value="" >
                        <label data-error="wrong" data-success="right" for="defaultForm-text">Введите имя пользователя</label>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="email" id="defaultForm-email" name="email" class="form-control validate" value="" >
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Введите email</label>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="text" id="defaultForm-text" name="title" class="form-control validate" value="" >
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Введите текст</label>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-default" type="sumbit">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTaskAdd" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Создать задачу</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Создать задачу">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="createTaskForm" role="form" method="POST" data-method="create" action="">
                <div class="modal-body mx-3">
                    <input type="hidden" id="defaultForm-text" name="id" class="form-control validate" value="" >
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input type="text" id="defaultForm-text" name="user_name" class="form-control validate" value="" >
                        <label data-error="wrong" data-success="right" for="defaultForm-text">Введите имя пользователя</label>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="email" id="defaultForm-email" name="email" class="form-control validate" value="" >
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Введите email</label>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="text" id="defaultForm-text" name="title" class="form-control validate" value="" >
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Введите текст</label>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-default" type="sumbit">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="autorizationUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Авторизация</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Логин">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="autorizationForm" role="form" method="POST" action="">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-envelope prefix grey-text"></i>
                        <input type="text" id="defaultForm-text" name="login" class="form-control validate">
                        <label data-error="wrong" data-success="right" for="defaultForm-text">Введите логин</label>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="password" id="defaultForm-pass" name="password" class="form-control validate">
                        <label data-error="wrong" data-success="right" for="defaultForm-pass">Введите пароль</label>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-default" type="sumbit">Логин</button>
                </div>
            </form>
        </div>
    </div>
</div>
