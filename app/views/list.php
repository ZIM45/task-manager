<?php include('header.php'); ?>
<div class="container">
    <div class=row">
        <a href="" class="btn btn-primary float-right btn-rounded mb-4" data-toggle="modal" data-target="#modalTaskAdd">Добавить задачу</a>
        <a href="" class="btn btn-primary float-right btn-rounded mb-4" data-toggle="modal" data-target="#autorizationUserModal">Авторизация</a>
        <?php if($_SESSION["user_id"]) { ?>
                  <a href="" class="btn btn-primary float-right btn-rounded mb-4" id="logoutButton">Выйти</a>
        <?php } ?>
    </div>
<div class="row">
    <table class="table" id="dataTable">
        <thead class="black white-text">
        <tr>
            <th scope="col"><a href="<?= $this->getSortUrl('id')?>"></a>id</a></th>
            <th scope="col"><a href="<?= $this->getSortUrl('user_name')?>">Имя пользователя</a></th>
            <th scope="col"><a href="<?= $this->getSortUrl('email')?>">Email</a></th>
            <th scope="col"><a href="<?= $this->getSortUrl('title')?>">Текст задачи</a></th>
            <th scope="col"><a href="<?= $this->getSortUrl('status')?>">Статус</a></th>
        </thead>
        <tbody>
        <?php foreach($this->data['data'] as $item) { ?>
            <tr>
                <th scope="row"><?=$item['id']?></th>
                <td><?=$item['user_name']?></td>
                <td><?=$item['email']?></td>
                <td><?=$item['title']?></td>
                <td><? echo ($item['status'] == 2) ?  'Выполнено' :  'Не выполнено'?></td>
                <td class="button-table">
                   <span>Выполнить</span>
                    <input class="completeButton" data-id="<?=$item['id']?>" type="checkbox" <?php if($item['status'] == 2) echo 'checked' ?>  name="myTextEditBox"  />
                </td>
                <?php if($_SESSION["user_id"]) { ?>
                <td class="button-table">
                    <input class="editButton" data-id="<?=$item['id']?>" type="button" name="myTextEditBox" value="Редактировать" />
                </td>
                <?php } ?>
                <td class="clean-table-text">
                    <? echo ($item['is_admin_edited']) ?  'Отредактировано администратором' :  ''?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <nav aria-label="">
        <ul class="pagination justify-content-center">
            <?php if ($this->data['page'] > 1) {  ?>
                <li class="page-item"><a class="page-link" href="<?=$this->getPageUrl($this->data['page']-1)?>">Предыдущая</a></li>
            <?php } ?>

            <?php if ($this->data['total_pages'] > 1 && $this->data['page']+1 <= $this->data['total_pages']) {  ?>
                <li class="page-item"><a class="page-link" href="<?=$this->getPageUrl($this->data['page']+1)?>">Следующая</a></li>
                <li class="page-item"><a class="page-link" href="<?=$this->getPageUrl($this->data['total_pages'])?>">Последняя</a></li>
            <?php } ?>
        </ul>
    </nav>
</div>
