<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 16:02
 */
class TaskView extends AbstractView {

    public function renderList($data)
    {
        $this->assign($data);
        echo $this->render('list');
    }


}