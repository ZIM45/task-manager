<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 11:49
 */

abstract class AbstractView {

    protected $data;
    protected $params;

    protected function render($template) {
        ob_start();
        require 'app/views/' . $template . '.php';
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }

    protected function assign($data) {
        $this->data = $data;
    }

    protected function getPageUrl($page = null) {
        $url = '';

        if(count($_GET) == 0 && $page) {
            return '?page='.$page;
        }

        foreach($_GET as $param => $value) {
            if ($page && $param == 'page') {
                $url.='?page='.$page;
            } else if(key($_GET) == $param) {
                $url.='?'.$param.'='.$value;
            } else {
                $url.='&'.$param.'='.$value;
            }
        }
        return $url;
    }

    protected function getSortUrl($field) {
        $url = '?page='.$this->data['page'].'&sort='.$field;
        if(isset($_GET['dir']) && $_GET['dir'] == 'asc') {
            $url.='&dir=desc';
        } else {
            $url.='&dir=asc';
        }
        return $url;
    }

}