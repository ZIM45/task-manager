<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 14:22
 */
class TaskController extends AbstractController {

    public function __construct()
    {
        $this->setViewClass(new TaskView());
    }

    protected function getFilterData()
    {
        $data['page'] = ($this->getRequestParam('page')) ? $this->getRequestParam('page') : 1;
        $data['limit'] = ($this->getRequestParam('limit')) ? $this->getRequestParam('limit') : 3;
        $data['dir'] = ($this->getRequestParam('dir')) ? $this->getRequestParam('dir') : 'asc';
        $data['sort'] = ($this->getRequestParam('sort')) ? $this->getRequestParam('sort') : 'id';
        return $data;
    }

    public function listAction()
    {
        $task = new Task();
        $pagination = new Pagination($task);
        $data = $pagination->getPaginationList($this->getFilterData());

        if($this->isAjax) {
            echo json_encode($data);
        } else {
            $this->getViewClass()->renderList($data);
        }
    }

    public function getTaskAction()
    {
        if($this->isAjax) {
            $id = $this->getRequestParam('id');
            $task = new Task($id);
            $data = $task->getData();
            echo json_encode($data);
        }
    }

    public function editAction()
    {
        if($this->isAjax) {
            $data = $this->getRequestBody();
            $task = new Task($this->getRequestParam('id'));

            if(strcmp($task->getData('title'), $data['title']) != 0) {
                $task->addData('is_admin_edited', 1);
            }

            foreach($data as $name => $value) {
                $task->addData($name, $value);
            }

            if(!$this->isAdminAction()){
                echo json_encode(array('message' => 'Ошибка! Необходимо авторизироваться для редактирования задачи'));
                return;
            }

            if($task->save()) {
                echo json_encode(array('message' => 'Задача обновлена!'));
                return;
            }

            echo json_encode(array('message' => 'Ошибка'));
        }
    }

    public function changeCompleteAction()
    {
        if($this->isAjax) {
            $id = $this->getRequestParam('id');
            $task = new Task($id);
            if($task->getData('status') == 1){
                $task->addData('status', 2);
            } else if ($task->getData('status') == 2) {
                $task->addData('status', 1);
            }
            $task->save();
        }
    }

    public function saveAction()
    {
        if($this->isAjax)
        {
            $data = $this->getRequestBody();
            $task = new Task($this->getRequestParam('id'));

            foreach($data as $name => $value) {
                $task->addData($name, $value);
            }

            if($task->save()) {
                echo json_encode(array('message' => 'Задача обновлена!'));
                return;
            }
            echo json_encode(array('message' => 'Ошибка'));
        }
    }
}