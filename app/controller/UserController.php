<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 14:21
 */
class UserController extends AbstractController {

    public function authAction() {
        if($this->isAjax){
            $responseData = new stdClass();
            $responseData->status = 'error';

            $username = $this->getRequestParam("login");
            $password = $this->getRequestParam("password");

            $user = new User();
            $userId = $user->ifExistByPass($username, $password);

            if($userId){
                $responseData->status = 'success';
            }

            $this->saveSession($userId);
            $this->setResponse(json_encode($responseData));
            $this->showJsonResponse($this->getResponse());
        }

    }

    public function logoutAction()
    {
        if($_SESSION["user_id"]) {
            unset($_SESSION["user_id"]);
        }
        setcookie("sid", "");
        return;
    }

    protected function saveSession($userId, $http_only = true, $days = 7)
    {
        $_SESSION["user_id"] = $userId;
        $sid = session_id();

        $expire = time() + $days * 24 * 3600;
        $domain = ""; // default domain
        $secure = false;
        $path = "/";

        setcookie("sid", $sid, $expire, $path, $domain, $secure, $http_only);
    }
}