<?php
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 14:12
 */
abstract class AbstractController {

    protected $viewClass;
    protected $isAjax;
    protected $response;


    public function isAdminAction()
    {
        if($_SESSION["user_id"]) {
            return true;
        }
        return false;

    }


    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }

    public function setIsAjax($isAjax)
    {
        $this->isAjax = $isAjax;
    }


    public function getViewClass()
    {
        return $this->viewClass;
    }


    public function setViewClass($viewClass)
    {
        $this->viewClass = $viewClass;
    }

    // TO-DO move to new Request class
    public function getRequestParam($param)
    {
        if (array_key_exists($param, $_REQUEST)) {
            return trim($_REQUEST[$param]);
        }
        return null;
    }

    // TO-DO move to new Request class
    public function getRequestBody()
    {
        if(isset($_POST)){
            return $_POST;
        }
    }

    // TO-DO move to new Request class
    public function showJsonResponse()
    {
        header("Content-Type: application/json; charset=UTF-8");
        echo $this->response;
    }

}

