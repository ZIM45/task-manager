<?php
include('app/config.php');
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 15:16
 */

spl_autoload_register('Autoloader::loadControllers');
spl_autoload_register('Autoloader::loadModels');
spl_autoload_register('Autoloader::loadViews');


class Autoloader
{
    public static function loadControllers($class)
    {
        $file = 'app/controller/' . str_replace('\\', '/', $class) . '.php';
        if (is_file($file)) {
            include $file;
        } else {
            return false;
        }
    }

    public static function  loadModels($class)
    {
        $file = 'app/model/' . str_replace('\\', '/', $class) . '.php';
        if (is_file($file)) {
            include $file;
        } else {
            return false;
        }
    }

    public static function  loadViews($class)
    {
        $file = 'app/view/' . str_replace('\\', '/', $class) . '.php';
        if (is_file($file)) {
            include $file;
        } else {
            return false;
        }
    }


}