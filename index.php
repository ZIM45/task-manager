<?php
include('autoload.php');
/**
 * Created by PhpStorm.
 * User: oleksandr.tolkach
 * Date: 15.06.2020
 * Time: 11:43
 */

if($_SERVER['REQUEST_URI']) {

    $url = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], "?"));
    if(!strpos($_SERVER['REQUEST_URI'], '?')) {
        $url = $_SERVER['REQUEST_URI'];
    }

    session_start();
    if (!empty($_COOKIE['sid'])) {
        session_id($_COOKIE['sid']);
    }
    setcookie("sid", "");

    $path = trim($url, '/');
    if(!$path){
        $loadedController = new TaskController();
        $loadedController->listAction();
        return;
    }

    $path = explode('/', $path);
    $ajax = false;

    foreach($path as $key => $param){
        if ($param == 'ajax') {
            $ajax = true;
            array_shift($path);
        }
    }

    $controller = $path[0];
    $action = $path[1];

    switch ($controller) {
        case 'task':
            $loadedController = new TaskController();
            break;
        case 'user':
            $loadedController = new UserController();
            break;
    }


    $loadedController->setIsAjax($ajax);
    call_user_func(array($loadedController, strtolower($action).'Action'));


}